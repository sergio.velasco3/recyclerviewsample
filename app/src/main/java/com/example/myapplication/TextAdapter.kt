package com.example.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ListView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.myapplication.databinding.HolderListBinding

class TextAdapter() : RecyclerView.Adapter<TextAdapter.VistaCelda>() {

    var miLista = ArrayList<String>()

    inner class VistaCelda(val binding: HolderListBinding) : ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VistaCelda {
        val inflater = LayoutInflater.from(parent.context)
        val binding = HolderListBinding.inflate(inflater, parent, false)
        val celda = VistaCelda(binding)
        return celda
    }

    override fun getItemCount(): Int {
        return miLista.size
    }

    override fun onBindViewHolder(holder: VistaCelda, position: Int) {
        val texto = miLista[position]
        holder.binding.textView.text = texto
    }

    fun llenarLista(lista: List<String>) {
        miLista.addAll(lista)
        notifyDataSetChanged()
    }

    fun addItem(texto: String) {
        miLista.add(texto)
        notifyItemInserted(miLista.size - 1)//posicion elemento
        //notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        miLista.removeAt(position)
        //notifyDataSetChanged()
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, miLista.size);
    }


}















