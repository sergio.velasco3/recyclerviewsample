package com.example.myapplication

data class Persona(
    var nombre: String,
    var apellidos: String,
    val edad: Int
)
