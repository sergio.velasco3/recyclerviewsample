package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.Person
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val persona = Persona("Sergio", "Velasco", 35)
        persona.nombre = "Alberto"

        val listado = ArrayList<Persona>()
        listado.add(persona)
        listado.add(Persona("Antonio", "Sánchez", 33))
        listado.add(Persona("Pedro", "Pérez", 32))
        listado.add(Persona("María", "García", 31))
        listado.add(Persona("Eva", "Ruiz", 30))
        listado.add(Persona("Ana", "Martínez", 29))

        val layoutManager = LinearLayoutManager(this)
        binding.recycler.layoutManager = layoutManager
        val adapter = PersonaAdaptador()
        binding.recycler.adapter = adapter

        adapter.addLista(listado)

        binding.butAdd.setOnClickListener {
            val persona = Persona("qwer","asdfas",34)
            adapter.addPersona(persona)
        }

        /*
        val array = ArrayList<String>()
        array.add("Blanco")
        array.add("Rojo")
        array.add("Morado")
        array.add("Azul")
        array.add("Amarillo")
        array.add("Naranja")
        array.add("Blanco")
        array.add("Rojo")
        array.add("Morado")
        array.add("Azul")
        array.add("Amarillo")
        array.add("Naranja")
        array.add("Blanco")
        array.add("Rojo")
        array.add("Morado")
        array.add("Azul")
        array.add("Amarillo")
        array.add("Naranja")
        array.add("Blanco")
        array.add("Rojo")
        array.add("Morado")
        array.add("Azul")
        array.add("Amarillo")
        array.add("Naranja")
        array.add("Blanco")
        array.add("Rojo")
        array.add("Morado")
        array.add("Azul")
        array.add("Amarillo")
        array.add("Naranja")

        val llm = LinearLayoutManager(this)
        binding.recycler.layoutManager = llm

        val adapter = TextAdapter()
        binding.recycler.adapter = adapter

        adapter.llenarLista(array)

        binding.butAdd.setOnClickListener {
            adapter.addItem("Negro")
        }

        binding.butDelete.setOnClickListener {
            adapter.removeItem(0)
        }

         */
    }
}