package com.example.myapplication

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.myapplication.databinding.HolderPersonaBinding

class PersonaAdaptador : Adapter<PersonaAdaptador.CeldaPersona>() {

    val listado = ArrayList<Persona>()
    inner class CeldaPersona(val binding: HolderPersonaBinding) : ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CeldaPersona {
        val inflater = LayoutInflater.from(parent.context)
        val binding = HolderPersonaBinding.inflate(inflater, parent, false)
        val celda = CeldaPersona(binding)
        return celda
        //return CeldaPersona(HolderPersonaBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int {
        return listado.size
    }

    override fun onBindViewHolder(holder: CeldaPersona, position: Int) {
        val persona = listado[position]
        with(holder.binding) {
            tvName.text = persona.nombre
            tvSurname.text = persona.apellidos
            tvAge.text = persona.edad.toString()
        }
        val context = holder.itemView.context
        holder.binding.tvAge.setOnClickListener {
            Toast.makeText(context, persona.edad.toString(), Toast.LENGTH_SHORT).show()

            /*val intent = Intent(context, MainActivity::class.java)
            intent.putExtra("edad", persona.edad)
            context.startActivity(intent)
            */

        }
       // holder.binding.tvName.text = persona.nombre

    }

    fun addLista(list: ArrayList<Persona>) {
        listado.addAll(list)
        notifyItemRangeInserted(0, listado.size)
//        notifyDataSetChanged()
    }

    fun addPersona(persona: Persona) {
        listado.add(persona)
        notifyItemInserted(listado.size - 1)
    }
}















